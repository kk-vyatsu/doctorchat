package ru.kolpkir.doctorchat.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kolpkir.doctorchat.dao.IUserRepository;
import ru.kolpkir.doctorchat.model.Role;
import ru.kolpkir.doctorchat.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserService implements IUserService {
    @Autowired
    private Environment env;

    private IUserRepository userRepository;

    @Override
    public boolean create(User user) {
        return userRepository.create(user);
    }

    @Override
    @Transactional
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Transactional
    public List<User> getDoctors() {
        return userRepository.getDoctors();
    }

    @Override
    public User getById(UUID id) {
        return userRepository.getById(id).orElse(null);
    }

    @Override
    public User getByLogin(String login) {
        return userRepository.getByLogin(login).orElse(null);
    }

    @Override
    public boolean isUser(UUID id) {
        return userRepository.getById(id).isPresent();
    }

    @Override
    public boolean isUserByLogin(String login) {
        return userRepository.getByLogin(login).isPresent();
    }

    @Override
    public boolean update(User user) {
        return userRepository.update(user);
    }

    @Override
    public boolean delete(UUID id) {
        return userRepository.delete(id);
    }

    public void createDefaultAdmin() {
        String defaultAdminLogin = env.getProperty("users.default.admin.login");
        String defaultAdminEmail = env.getProperty("users.default.admin.email");
        String defaultAdminPass = env.getProperty("users.default.admin.pass");
        if (defaultAdminLogin == null || defaultAdminEmail == null || defaultAdminPass == null)
            return;
        if (!isUserByLogin(defaultAdminLogin)) {
            create(new User(UUID.randomUUID(), Role.ADMIN, "Default", "", "Admin",
                    defaultAdminLogin, defaultAdminEmail, defaultAdminPass, true));
        }
    }
}
