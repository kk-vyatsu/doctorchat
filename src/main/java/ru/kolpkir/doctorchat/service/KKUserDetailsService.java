package ru.kolpkir.doctorchat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.kolpkir.doctorchat.dao.UserRepository;
import ru.kolpkir.doctorchat.model.Role;
import ru.kolpkir.doctorchat.model.User;

import java.util.Arrays;
import java.util.List;

@Component
public class KKUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.getByLogin(username).orElse(null);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        String role;
        switch (user.getRole()) {
            case ADMIN: role = "ADMIN"; break;
            case DOCTOR: role = "DOCTOR"; break;
            default: role = "USER"; break;
        }
        List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority(role));

        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), authorities);
    }
}
