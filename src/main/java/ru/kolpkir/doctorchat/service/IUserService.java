package ru.kolpkir.doctorchat.service;

import ru.kolpkir.doctorchat.model.User;

import java.util.List;
import java.util.UUID;

public interface IUserService {
    /**
     * Создаёт нового пользователя
     * @param user - пользователь для создания
     * @return - true если пользователь создан, иначе false
     */
    boolean create(User user);

    /**
     * Возвразает список всех пользователей
     * @return - список клиентов
     */
    List<User> getAll();

    /**
     * Возвращает пользователя по его ID
     * @param id - ID пользователя
     * @return - объект пользователя с заданным ID
     */
    User getById(UUID id);

    /**
     * Возвращает пользователя по его логину
     * @param login - логин пользователя
     * @return - объект пользователя с заданным логином
     */
    User getByLogin(String login);

    /**
     * Проверяет, существует ли пользователь с указанным ID
     * @param id - ID пользователя
     * @return - true если пользователь существует, иначе false
     */
    boolean isUser(UUID id);

    /**
     * Проверяет, существует ли пользователь с указанным логином
     * @param login - логин пользователя
     * @return - true если пользователь существует, иначе false
     */
    boolean isUserByLogin(String login);

    /**
     * Обновляет пользователя с заданным ID
     * @param user - новые данные пользователя
     * @return - true если данные были обновлены, иначе false
     */
    boolean update(User user);

    /**
     * Удаляет пользователя с заданным ID
     * @param id - id пользователя, которого нужно удалить
     * @return - true если пользователь был удален, иначе false
     */
    boolean delete(UUID id);
}
