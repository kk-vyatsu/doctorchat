package ru.kolpkir.doctorchat.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kolpkir.doctorchat.dao.MessageRepository;
import ru.kolpkir.doctorchat.model.Message;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class MessageService {
    private MessageRepository messageRepository;

    public boolean create(Message message) {
        return messageRepository.create(message);
    }

    public Optional<Message> getById(UUID id) {
        return messageRepository.getById(id);
    }

    public List<Message> getAllInChat(UUID chatId) {
        return messageRepository.getAllInChat(chatId);
    }

    public boolean delete(UUID id) {
        return messageRepository.delete(id);
    }
}
