package ru.kolpkir.doctorchat.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kolpkir.doctorchat.dao.ChatRepository;
import ru.kolpkir.doctorchat.model.Chat;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class ChatService {
    private ChatRepository chatRepository;

    public boolean create(Chat chat) {
        return chatRepository.create(chat);
    }

    public Optional<Chat> getById(UUID id) {
        return chatRepository.getById(id);
    }

    public List<Chat> getAll() {
        return chatRepository.getAll();
    }

    public List<Chat> getAllPatient(UUID patientId) {
        return chatRepository.getAllPatient(patientId);
    }

    public List<Chat> getAllDoctor(UUID doctorId) {
        return chatRepository.getAllDoctor(doctorId);
    }

    public boolean delete(UUID id) {
        return chatRepository.delete(id);
    }
}
