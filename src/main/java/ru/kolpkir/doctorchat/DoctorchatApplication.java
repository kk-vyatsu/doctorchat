package ru.kolpkir.doctorchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoctorchatApplication {

    public static void main(String[] args) {
        SpringApplication.run(DoctorchatApplication.class, args);
    }

}
