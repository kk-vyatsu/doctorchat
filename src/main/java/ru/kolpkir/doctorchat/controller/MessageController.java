package ru.kolpkir.doctorchat.controller;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.kolpkir.doctorchat.model.*;
import ru.kolpkir.doctorchat.service.ChatService;
import ru.kolpkir.doctorchat.service.MessageService;
import ru.kolpkir.doctorchat.service.UserService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/message")
@AllArgsConstructor
public class MessageController {
    private final UserService userService;
    private final MessageService messageService;
    private final ChatService chatService;

    @PostMapping("/create")
    public Message createMessage(@RequestBody OpenMessage message, Authentication authentication) {
        User user = userService.getByLogin(authentication.getName());
        if (user.getId() != message.getUserId())
            return null;
        Message crMessage = message.toMessage();
        boolean created = messageService.create(crMessage);
        return created ? messageService.getById(crMessage.getId()).orElse(null) : null;
    }

    @GetMapping("/get-all-in-chat")
    public List<Message> getAllInChat(@RequestParam UUID id, Authentication authentication) {
        User user = userService.getByLogin(authentication.getName());
        Chat chat = chatService.getById(id).orElse(null);
        if ((user.getRole() == Role.DOCTOR && chat.getDoctorId() != user.getId()) || (user.getRole() == Role.USER && chat.getPatientId() != user.getId()))
            return null;
        return messageService.getAllInChat(id);
    }
}
