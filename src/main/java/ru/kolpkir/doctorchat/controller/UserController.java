package ru.kolpkir.doctorchat.controller;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.kolpkir.doctorchat.model.CleanUser;
import ru.kolpkir.doctorchat.model.OpenUser;
import ru.kolpkir.doctorchat.model.User;
import ru.kolpkir.doctorchat.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/get-all")
    public List<User> getAllUsers() {
        return userService.getAll();
    }

    @GetMapping("/get-doctors")
    public List<CleanUser> getDoctors() {
        List<User> doctors = userService.getDoctors();
        List<CleanUser> cleanDoctors = new ArrayList<CleanUser>();
        for (User doctor : doctors) {
            cleanDoctors.add(doctor.toCleanUser());
        }
        return cleanDoctors;
    }

    @GetMapping("/create-default-admin")
    public String createDefaultAdmin() {
        userService.createDefaultAdmin();
        return "Success";
    }

    @PostMapping("/create")
    public CleanUser createUser(@RequestBody OpenUser user) {
        boolean created = userService.create(user.toNewUser());
        return created ? userService.getByLogin(user.getLogin()).toCleanUser() : null;
    }

    @PostMapping("/create-doctor")
    public CleanUser createDoctor(@RequestBody OpenUser user) {
        boolean created = userService.create(user.toNewDoctor());
        return created ? userService.getByLogin(user.getLogin()).toCleanUser() : null;
    }

    @PostMapping("/create-admin")
    public CleanUser createAdmin(@RequestBody OpenUser user) {
        boolean created = userService.create(user.toNewAdmin());
        return created ? userService.getByLogin(user.getLogin()).toCleanUser() : null;
    }

    @PutMapping("/update")
    public void updateUser(@RequestBody OpenUser user, Authentication authentication) {
        User oldUser = userService.getByLogin(authentication.getName());
        User updatedUser = new User(oldUser.getId(), oldUser.getRole(), user.getFirstName(), user.getPatronymic(),
                user.getLastName(), user.getLogin(), user.getEmail(), user.getPassword(), oldUser.getActive());
        userService.update(updatedUser);
    }

    @GetMapping("/get-my-uuid")
    public UUID getMyUUID(Authentication authentication) {
        return userService.getByLogin(authentication.getName()).getId();
    }

    @GetMapping("/get-by-uuid")
    public CleanUser getByUUID(@RequestParam UUID id) {
        return userService.getById(id).toCleanUser();
    }

    @GetMapping("/get-by-login")
    public CleanUser getByLogin(@RequestParam String login) {
        return userService.getByLogin(login).toCleanUser();
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam UUID id) {
        userService.delete(id);
    }
}
