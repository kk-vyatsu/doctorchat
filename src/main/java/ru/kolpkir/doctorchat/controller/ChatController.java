package ru.kolpkir.doctorchat.controller;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.kolpkir.doctorchat.model.Chat;
import ru.kolpkir.doctorchat.model.OpenChat;
import ru.kolpkir.doctorchat.model.Role;
import ru.kolpkir.doctorchat.model.User;
import ru.kolpkir.doctorchat.service.ChatService;
import ru.kolpkir.doctorchat.service.UserService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/chat")
@AllArgsConstructor
public class ChatController {
    private final UserService userService;
    private final ChatService chatService;

    @PostMapping("/create")
    public Chat createChat(@RequestBody OpenChat chat, Authentication authentication) {
        User user = userService.getByLogin(authentication.getName());
        if (user.getRole() != Role.USER || user.getId() != chat.getPatientId())
            return null;
        Chat crChat = chat.toChat();
        boolean created = chatService.create(crChat);
        return created ? chatService.getById(crChat.getId()).orElse(null) : null;
    }

    @GetMapping("get-all-my-chats")
    public List<Chat> getAllMyChats(Authentication authentication) {
        User user = userService.getByLogin(authentication.getName());
        if (user.getRole() == Role.DOCTOR)
            return chatService.getAllDoctor(user.getId());
        else if (user.getRole() == Role.USER)
            return chatService.getAllPatient(user.getId());
        return null;
    }

    @GetMapping("/get-all")
    public List<Chat> getAll() {
        return chatService.getAll();
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam UUID id) {
        chatService.delete(id);
    }
}
