package ru.kolpkir.doctorchat.dao;

import ru.kolpkir.doctorchat.model.User;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Transactional
public interface IUserRepository {
    boolean create(User user);
    List<User> getAll();
    List<User> getDoctors();
    Optional<User> getById(UUID id);
    Optional<User> getByLogin(String login);
    boolean update(User user);
    boolean delete(UUID id);
}
