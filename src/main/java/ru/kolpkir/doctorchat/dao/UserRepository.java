package ru.kolpkir.doctorchat.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import ru.kolpkir.doctorchat.model.Role;
import ru.kolpkir.doctorchat.model.User;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class UserRepository implements IUserRepository {
    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public boolean create(User user) {
        Session session = sessionFactory.getCurrentSession();
        try {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            session.persist(user);
            session.flush();
        } catch (HibernateException e) {
            return false;
        }
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from user").list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getDoctors() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from user where role = :role").setParameter("role", Role.DOCTOR.toString()).list();
    }

    @Override
    @Transactional
    public Optional<User> getById(UUID id) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class, id);
        return Optional.ofNullable(user);
    }

    @Override
    @Transactional
    public Optional<User> getByLogin(String login) {
        Session session = sessionFactory.getCurrentSession();
        List users = session.createQuery("from user where login = :login").setParameter("login", login).getResultList();
        if (users.size() > 0) {
            User user = (User) session.createQuery("from user where login = :login").setParameter("login", login).getSingleResult();
            return Optional.ofNullable(user);
        }
        return Optional.empty();
    }

    @Override
    @Transactional
    public boolean update(User user) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.update(user);
            session.flush();
        } catch (HibernateException e) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean delete(UUID id) {
        Session session = sessionFactory.getCurrentSession();
        try {
            User user = getById(id).orElse(null);
            if (user == null)
                return false;
            session.delete(user);
            session.flush();
        } catch (HibernateException e) {
            return false;
        }
        return true;
    }
}
