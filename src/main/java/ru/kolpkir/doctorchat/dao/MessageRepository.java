package ru.kolpkir.doctorchat.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kolpkir.doctorchat.model.Message;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class MessageRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public boolean create(Message message) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.persist(message);
            session.flush();
        } catch (HibernateException e) {
            return false;
        }
        return true;
    }

    @Transactional
    public Optional<Message> getById(UUID id) {
        Session session = sessionFactory.getCurrentSession();
        Message message = session.get(Message.class, id);
        return Optional.ofNullable(message);
    }

    @SuppressWarnings("unchecked")
    public List<Message> getAllInChat(UUID chatId) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from message where chatId = :chatId").setParameter("chatId", chatId).list();
    }

    @Transactional
    public boolean delete(UUID id) {
        Session session = sessionFactory.getCurrentSession();
        try {
            Message message = getById(id).orElse(null);
            if (message == null)
                return false;
            session.delete(message);
            session.flush();
        } catch (HibernateException e) {
            return false;
        }
        return true;
    }
}
