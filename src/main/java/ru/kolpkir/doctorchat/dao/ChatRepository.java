package ru.kolpkir.doctorchat.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kolpkir.doctorchat.model.Chat;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class ChatRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public boolean create(Chat chat) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.persist(chat);
            session.flush();
        } catch (HibernateException e) {
            return false;
        }
        return true;
    }

    @Transactional
    public Optional<Chat> getById(UUID id) {
        Session session = sessionFactory.getCurrentSession();
        Chat chat = session.get(Chat.class, id);
        return Optional.ofNullable(chat);
    }

    @SuppressWarnings("unchecked")
    public List<Chat> getAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from chat").list();
    }

    @SuppressWarnings("unchecked")
    public List<Chat> getAllPatient(UUID patientId) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from chat where patientId = :patientId").setParameter("patientId", patientId).list();
    }

    @SuppressWarnings("unchecked")
    public List<Chat> getAllDoctor(UUID doctorId) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from chat where doctorId = :doctorId").setParameter("doctorId", doctorId).list();
    }

    @Transactional
    public boolean delete(UUID id) {
        Session session = sessionFactory.getCurrentSession();
        try {
            Chat chat = getById(id).orElse(null);
            if (chat == null)
                return false;
            session.delete(chat);
            session.flush();
        } catch (HibernateException e) {
            return false;
        }
        return true;
    }
}
