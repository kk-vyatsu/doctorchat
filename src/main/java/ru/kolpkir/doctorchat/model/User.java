package ru.kolpkir.doctorchat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "user")
@Table(name = "users")
public class User implements Serializable {
    @Id
    @Column(name = "id")
    @NotNull
    private UUID id;

    @Column(name = "role")
    @NotNull
    private Role role;

    @Column(name = "firstName")
    @NotNull
    private String firstName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "lastName")
    @NotNull
    private String lastName;

    @Column(name = "login", unique = true)
    @NotNull
    private String login;

    @Column(name = "email", unique = true)
    @NotNull
    private String email;

    @Column(name = "password")
    @NotNull
    private String password;

    @Column(name = "active", columnDefinition = "tinyint default 1")
    @NotNull
    private Boolean active;

    public CleanUser toCleanUser() {
        return new CleanUser(firstName, patronymic, lastName, email);
    }
}
