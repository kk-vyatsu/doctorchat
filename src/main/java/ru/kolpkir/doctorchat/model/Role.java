package ru.kolpkir.doctorchat.model;

public enum Role {
    ADMIN,
    DOCTOR,
    USER
}
