package ru.kolpkir.doctorchat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CleanUser {
    @NotNull
    private String firstName;
    private String patronymic;
    @NotNull
    private String lastName;
    @NotNull
    private String email;
}
