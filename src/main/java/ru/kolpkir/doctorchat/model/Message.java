package ru.kolpkir.doctorchat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.sql.Time;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "message")
@Table(name = "message")
public class Message {
    @Id
    @Column(name = "id")
    @NotNull
    private UUID id;

    @Column(name = "chatId")
    @NotNull
    private UUID chatId;

    @Column(name = "userId")
    @NotNull
    private UUID userId;

    @Column(name = "content", length = 500)
    @NotNull
    private String content;

    @Column(name = "time")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    private Time time;
}
