package ru.kolpkir.doctorchat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "chat")
@Table(name = "chat")
public class Chat {
    @Id
    @Column(name = "id")
    @NotNull
    private UUID id;

    @Column(name = "doctorId")
    @NotNull
    private UUID doctorId;

    @Column(name = "patientId")
    @NotNull
    private UUID patientId;

    public Chat(UUID doctorId, UUID patientId) {
        this.doctorId = doctorId;
        this.patientId = patientId;
        id = UUID.randomUUID();
    }
}
