package ru.kolpkir.doctorchat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OpenChat {
    private UUID doctorId;
    private UUID patientId;

    public Chat toChat() {
        return new Chat(UUID.randomUUID(), doctorId, patientId);
    }
}
