package ru.kolpkir.doctorchat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalTime;
import java.util.UUID;

@Data
@AllArgsConstructor
public class OpenMessage {
    private UUID chatId;
    private UUID userId;
    private String content;

    public Message toMessage() {
        return new Message(UUID.randomUUID(), chatId, userId, content, Time.valueOf(LocalTime.now()));
    }
}
