package ru.kolpkir.doctorchat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OpenUser {
    @NotNull
    private String firstName;
    private String patronymic;
    @NotNull
    private String lastName;
    @NotNull
    private String login;
    @NotNull
    private String email;
    @NotNull
    private String password;

    public User toNewUser() {
        return new User(UUID.randomUUID(), Role.USER, firstName, patronymic, lastName, login, email, password, true);
    }

    public User toNewDoctor() {
        return new User(UUID.randomUUID(), Role.DOCTOR, firstName, patronymic, lastName, login, email, password, true);
    }

    public User toNewAdmin() {
        return new User(UUID.randomUUID(), Role.ADMIN, firstName, patronymic, lastName, login, email, password, true);
    }
}
